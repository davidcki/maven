/**
 * POJO test. Automatically catched by Surefire plugin.
 * Class has to end in Test, methods have to start with test.
 */
public class HelloWorldTest {

    public void testHello() {
        System.out.println("Hello...");
    }

    public void testWorld() {
        System.out.println("World!");
    }
}
