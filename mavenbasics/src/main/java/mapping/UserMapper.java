package mapping;

import domain.UserCommand;
import domain.UserPOJO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface UserMapper {

    @Mapping(source = "name", target = "firstName")
    UserPOJO fromUserCommand(UserCommand uc);

    @Mapping(source = "firstName", target = "name")
    UserCommand fromUser(UserPOJO u);
}
